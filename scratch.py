# -*- coding: utf-8 -*-
"""
Created on Tue Mar  6 18:39:48 2018

@author: jonas
"""
from datetime import datetime, timezone

from zipline.api import record, symbol, order
from zipline import run_algorithm

import pandas as pd


def initialize(context):
    pass

def handle_data(context, data):
    order(symbol('AAPL'), 10)
    record(APPL=data.current(symbol('AAPL'), 'price'))
    
    
start = datetime(2016, 3, 1, tzinfo=timezone.utc)
end = datetime(2016, 4, 1, tzinfo=timezone.utc)
start = pd.Timestamp(start, tz='UTC', offset = 'C')
end = pd.Timestamp(end, tz='UTC', offset='C')

restults = run_algorithm(
        start=start,
        end=end,
        initialize=initialize,
        handle_data=handle_data,
        capital_base = 100000,
        data_frequency='daily')

print("Different")