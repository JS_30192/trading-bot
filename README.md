# Things I have learned in this LU

- Basics of GitLab
- Create .md files
    - Headers
    - Lists
        1. Listed
        2. Unlisted
- Upload files to GitLab
    - Directories can only be deleted after all folders are deleted
    - Every file has to be uploaded individually
- Choosing a trading strategy
    - How to make the code out of the internet run

# Naive Trading Strategy
The first naive trading strategy in the file scratch.py focusses on one stock.
The program buys and sells this stock with the symbol "AAPL" forth and back 
during the time that the programmer inserts. During the process the program
analyses many different performance variables such as risk and current
performance. All the numbers though, are results of gathered data by 
"finance.google.com". 

# Buy and Hold Strategy
The Buy and Hold strategy buys a single stock of all symbols that the user 
inserts.

# Homework LU 9
## Exercise 1: Relationship between ziplin and quantopian
Zipline open source library
Quantopian is a company that put man hours into the maintanance of zipline

## Exercise 2: set_slippage
It computes the impact of an order on the actual stockprize of a stock.

## Exercise 3: handle_data
In the documentation i found a different explanation. 
The function orders 100 stocks of the before inserted stock that has not been
order so far. In the end the variable .has_ordered, that has been false before,
will be set to true.